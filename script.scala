/* Abans d'executar aquest script, executar init-cluster.scala en el 
sparkling-shell per inicialitzar els serveis de H2O sobre el cluster de Spark.*/

// ---------------------------------------------------------------------------------------------
// Exemple SQL:
val dataPath = "/home/alex/SIO/dades/2001.csv" 
val airlinesData = new H2OFrame(new File(dataPath))
val flightsTable = asDataFrame(airlinesData)(sqlContext)

// Registrar el esquema com a vols:
flightsTable.registerTempTable("flights")

// Executar consulta SQL sobre el esquema creat:
val result = sqlContext.sql(
        """SELECT
        |flights.ArrDelay, flights.TailNum
        |FROM flights""".stripMargin)

// Transformar resultat a H2oFrame:
val bigDataFrame: H2OFrame = h2oContext.asH2OFrame(result)

// Mostrar per GroupBy:
val res1 = result.groupBy("Month").count().show()