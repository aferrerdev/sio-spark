// Initialize H2O services on top of Spark cluster
import org.apache.spark.h2o._
val h2oContext = H2OContext.getOrCreate(sc)
import h2oContext._
import h2oContext.implicits._
import org.apache.spark.examples.h2o._
import java.io.File
import sqlContext.implicits._