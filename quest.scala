// Quants models d’avió hi ha? Relacionar el model d’avio amb el retard.
val airlinesData = new H2OFrame("plane_data.hex")
val PlaneTable = asDataFrame(airlinesData)(sqlContext)

// Registrar el esquema:
PlaneTable.registerTempTable("planedata")

// Executar consulta SQL sobre el esquema creat:
val result = sqlContext.sql(
        """SELECT planedata.model, COUNT(planedata.model) AS numAvions
        |FROM planedata GROUP BY planedata.model""".stripMargin)

// Contar models:
result.count

result.registerTempTable("result")

// Executar consulta SQL sobre el esquema creat:
val res1 = sqlContext.sql(
        """SELECT SUM(result.numAvions) AS Total
        |FROM result""".stripMargin)

res1.count
res1.show

// Transformar resultat a H2oFrame:
val bigDataFrame: H2OFrame = h2oContext.asH2OFrame(result)

