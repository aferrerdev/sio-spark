// Quins són els 10 models d'avió que tenen més retard (ArrDelay)? 

// 1. Consultar els tailNum que tenen retards.
//val dataPath = "/home/alex/SIO/dades/2001.csv" 
//val airlinesData = new H2OFrame(new File(dataPath))
val airlinesData = new H2OFrame("X1997.hex")
val flightsTable = asDataFrame(airlinesData)(sqlContext)

// Registrar el esquema com a vols:
flightsTable.registerTempTable("flights")

// Executar consulta SQL sobre el esquema creat:
val result = sqlContext.sql(
        """SELECT
        |flights.TailNum, SUM(flights.ArrDelay) AS ArrDelayTotal
        |FROM flights WHERE flights.ArrDelay > 0 GROUP BY flights.TailNum""".stripMargin)

// Resultat que ens mostra els avions i els seus retards acumulats.
// val res1 = result.groupBy("TailNum").sum("ArrDelay")
// Creem taula sql amb el resultat
result.registerTempTable("avionsresum")

// Quants models d’avió hi ha? Relacionar el model d’avio amb el retard.
val planeData = new H2OFrame(new File("/home/alex/SIO/dades/plane-data.csv"))
val PlaneTable = asDataFrame(planeData)(sqlContext)

// Registrar el esquema:
PlaneTable.registerTempTable("planedata")

// Fer un select a la taula d'avions per a veure quin model te cada TailNum.
val res2 = sqlContext.sql(
        """SELECT planedata.C1, planedata.C5
        |FROM planedata""".stripMargin)

// Registrar taula amb TailNum (C1), model (C5)
res2.registerTempTable("restable")

// Fer JOIN entre taula 'restable' i 'avionsresum'
val res3 = sqlContext.sql(
		"""SELECT
		| avionsresum.TailNum AS TailNum, avionsresum.ArrDelayTotal AS ArrDelayTotal,
		| restable.C5 AS model
		| FROM avionsresum
		| JOIN restable ON avionsresum.TailNum=restable.C1
		| """.stripMargin)

// Agrupar per model d'avió i sumar el retard d'arribada.
val restotal = res3.groupBy("model").sum("ArrDelayTotal")

restotal.sort($"sum(ArrDelayTotal)".desc).show