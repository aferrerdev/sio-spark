// Ajuntem resultats finals quest8.scala

val planesData = new H2OFrame("totalvolscancelacionsmodeledat87_08.hex")
val planesTable = asDataFrame(planesData)(sqlContext)

planesTable.registerTempTable("result")

val result = sqlContext.sql(
        """SELECT result.model, SUM(result.cancelats) AS cancelats,
        | SUM(result.volsTotals) AS volsTotals
        | FROM result
        | WHERE result.edat < 100
        | GROUP BY result.model""".stripMargin)

result.registerTempTable("res")

val res = sqlContext.sql(
        """SELECT res.model, res.cancelats, res.volsTotals, (res.cancelats/res.volsTotals) AS Norm
        | FROM res ORDER BY Norm DESC""".stripMargin)

res.count
res.show

val bigDataFrame: H2OFrame = h2oContext.asH2OFrame(res)
