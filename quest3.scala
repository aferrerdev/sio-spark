val airlinesData = new H2OFrame("X1997.hex")
val flightsTable = asDataFrame(airlinesData)(sqlContext)

// Registrar el esquema com a vols:
flightsTable.registerTempTable("flights")

// Executar consulta SQL sobre el esquema creat:
val result1 = sqlContext.sql(
        """SELECT
        | flights.Origin, COUNT(flights.Cancelled) AS TotalCanc
        | FROM flights WHERE flights.Cancelled = 1 GROUP BY
        | flights.Origin ORDER BY TotalCanc DESC""".stripMargin)

result1.registerTempTable("resultat1")

// Executar consulta SQL sobre el esquema creat:
val result2 = sqlContext.sql(
        """SELECT
        | flights.Origin, COUNT(flights.Origin) AS TotalVols
        | FROM flights GROUP BY flights.Origin""".stripMargin)

result2.registerTempTable("resultat2")

// Fer JOIN entre taula 'resultat1' i 'resultat2'
val res3 = sqlContext.sql(
		"""SELECT
		| resultat1.Origin, resultat1.TotalCanc, resultat2.TotalVols, 
		| resultat1.TotalCanc/resultat2.TotalVols AS Normalized
		| FROM resultat1
		| JOIN resultat2 ON resultat1.Origin=resultat2.Origin
		| ORDER BY Normalized DESC""".stripMargin)

res3.show()

// Mostrar resultat H20.
//val bigDataFrame: H2OFrame = h2oContext.asH2OFrame(result)


