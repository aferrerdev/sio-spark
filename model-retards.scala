// Preparar taula per a diverses prediccions.

val planesData = new H2OFrame("plane_data.hex")
val planesTable = asDataFrame(planesData)(sqlContext)

val airlinesData = new H2OFrame("X2007.hex")
val flightsTable = asDataFrame(airlinesData)(sqlContext)

// Taules Temporals per a SQL.
planesTable.registerTempTable("planes")
flightsTable.registerTempTable("flights")

val result1 = sqlContext.sql(
        """SELECT flights.*,
        | (flights.Year-planes.Year) AS edat, planes.model, planes.manufacturer,
        | CASE WHEN flights.ArrDelay > 0 THEN 'YES' ELSE 'NO' END AS ArrRetard,
	| CASE WHEN flights.DepDelay > 0 THEN 'YES' ELSE 'NO' END AS DepRetard
        | FROM flights
        | JOIN planes ON planes.TailNum = flights.TailNum""".stripMargin)

result1.registerTempTable("res1")

// Filtrem possibles errors amb edats.
val result2 = sqlContext.sql(
	"""SELECT *
	| FROM res1
	| WHERE res1.edat < 100""".stripMargin)

val bigDataFrame: H2OFrame = h2oContext.asH2OFrame(result2)
