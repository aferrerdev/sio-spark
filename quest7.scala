// Quest 7: Nombre de vols ped edat de l'avió

val planesData = new H2OFrame("plane_data.hex")
val airlinesData = new H2OFrame("X1997.hex")

val planesTable = asDataFrame(planesData)(sqlContext)
val flightsTable = asDataFrame(airlinesData)(sqlContext)

// Taules Temporals per a SQL.
planesTable.registerTempTable("planes")
flightsTable.registerTempTable("flights")

// De la taula flights agafarem Year, TailNum i Cancelled
val result = sqlContext.sql(
        """SELECT flights.Year AS yearFlight, flights.TailNum
        |FROM flights""".stripMargin)

result.registerTempTable("vols")

// De la taula de plane_data agafarem el Year i el TailNum.
val  res1 = sqlContext.sql(
        """SELECT (vols.yearFlight-planes.Year) AS edat, vols.TailNum 
        | FROM vols
        | JOIN planes ON planes.TailNum = vols.TailNum""".stripMargin)

res1.registerTempTable("res1")

val  res2 = sqlContext.sql(
        """SELECT res1.edat, COUNT(res1.TailNum) AS vols
        | FROM res1 WHERE edat >= 0
        | GROUP BY res1.edat ORDER BY res1.edat ASC""".stripMargin)

res2.show(35)
val bigDataFrame: H2OFrame = h2oContext.asH2OFrame(res2)