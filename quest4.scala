val airlinesData = new H2OFrame("X1997.hex")
val flightsTable = asDataFrame(airlinesData)(sqlContext)

// Registrar el esquema com a vols:
flightsTable.registerTempTable("flights")

// Executar consulta SQL sobre el esquema creat:
val result = sqlContext.sql(
        """SELECT
        |flights.Origin, COUNT(flights.DepDelay) AS NumDepDelays, COUNT(flights.Origin)
        |FROM flights WHERE flights.DepDelay > 0 GROUP BY flights.Origin ORDER BY NumDepDelays DESC """.stripMargin)

result.count()
result.show()

// Executar consulta SQL sobre el esquema creat:
val result = sqlContext.sql(
        """SELECT
        |flights.Origin, COUNT(flights.ArrDelay) AS NumDepDelays, COUNT(flights.Origin)
        |FROM flights WHERE flights.ArrDelay > 0 GROUP BY flights.Origin ORDER BY NumDepDelays DESC """.stripMargin)

result.count()
result.show()


