
// === Simple CSV Parser ===

// Path absolut de la carpeta on es troben les dades.
val dataPath = "/home/alex/SIO/dades/" 

def parseCSV(csvPath:String)={
	// Read the CSV file and convert textFile to RDD
	val csv = sc.textFile(csvPath)
	// split / clean data
	val headerAndRows = csv.map(line => line.split(",").map(_.trim))
	// Return header and rows of CSV.
	headerAndRows
}

val headerAndRows = parseCSV(dataPath+"plane-data.csv");
// get header
val header = headerAndRows.first
// filter out header (eh. just check if the first val matches the first header name)
val rows = headerAndRows.filter(_(0) != header(0)) //  Es igual a: return data


// splits to map (header/value pairs)
val maps = rows.map(splits => header.zip(splits).toMap)

println("Exemple de objecte: "+println(maps.first))

// filter out the user "me"
val res1 = maps.filter(map => map("Month") != "2")
val result = res1.filter(map => map("CancellationCode") == "D")

// print result
result.foreach(println)