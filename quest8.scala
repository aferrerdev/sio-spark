// Quest 6: Influeix l'edat de l'avió en les cancel·lacions?

val planesData = new H2OFrame("plane_data.hex")
val planesTable = asDataFrame(planesData)(sqlContext)

val airlinesData = new H2OFrame("X2007.hex")
val flightsTable = asDataFrame(airlinesData)(sqlContext)

// Taules Temporals per a SQL.
planesTable.registerTempTable("planes")
flightsTable.registerTempTable("flights")

// Mirar si l'edad de l'avio afecta a les cancel·lacions.
// El resultat que volem ens ha de mostrar una columna d'edat
// de l'avio i el nombre total de cancel·lacions


// De la taula flights agafarem Year, TailNum i Cancelled
val result = sqlContext.sql(
        """SELECT flights.Year AS yearFlight, flights.TailNum, flights.Cancelled
        |FROM flights""".stripMargin)

result.registerTempTable("cancelats")

// De la taula de plane_data agafarem el Year i el TailNum.
val res1 = sqlContext.sql(
        """SELECT (cancelats.yearFlight-planes.Year) AS edat, planes.model AS model, cancelats.Cancelled
        | FROM cancelats
        | JOIN planes ON planes.TailNum = cancelats.TailNum""".stripMargin)

res1.registerTempTable("res1")

val resultFinal = sqlContext.sql(
        """SELECT res1.model, res1.edat, SUM(res1.Cancelled) AS cancelats, COUNT(*) AS volsTotals 
        | FROM res1 WHERE res1.edat >= 0
        | GROUP BY res1.model, res1.edat ORDER BY res1.model, res1.edat""".stripMargin)

resultFinal.show
resultFinal.count


val bigDataFrame: H2OFrame = h2oContext.asH2OFrame(resultFinal)
